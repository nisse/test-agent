CC = gcc
CFLAGS = -g -O -Wall 
LOADLIBES = -lhogweed -lnettle

all: test-agent

check: all
	./testsuite.sh

.PHONY: check
