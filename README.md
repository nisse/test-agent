# test-agent

ssh-agent implementation intended for usage in tests.

The agent accepts private key to use (-k), socket name (-s), and
command to run with access to the agent. Forks, but lets the *child*
process take the agent role, while the parent process execs the
command to run. That way

```
test-agent -k key -s sock foo &
PID=$!
...
kill $!
```

kills the foo process, not the agent. Agent will also exit, due to a
pipe trick. In addition, parent of test-agent reliably gets the exit
code from the foo process.

The agent currently handles only a single connection, but could be
extended to handle concurrent connections if needed.
