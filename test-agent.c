/* test-agent.c

   PEM-related code copied from nettle/tools/pkcs1-conv.c, with
   following license notice:

   Copyright (C) 2005, 2009, Niels Möller, Magnus Holmgren
   Copyright (C) 2014 Niels Möller

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <sys/socket.h>
#include <sys/un.h>

#include <sys/select.h>

#include <nettle/base64.h>
#include <nettle/buffer.h>
#include <nettle/eddsa.h>
#include <nettle/macros.h>

#define MAX_REQUEST_SIZE 32000

/* See https://datatracker.ietf.org/doc/html/draft-miller-ssh-agent */
#define SSH_AGENT_FAILURE 5
#define SSH_AGENTC_REQUEST_IDENTITIES 11
#define SSH_AGENT_IDENTITIES_ANSWER 12
#define SSH_AGENTC_SIGN_REQUEST 13
#define SSH_AGENT_SIGN_RESPONSE 14

/* Length of an ed25519 public key blob, wrapped in an ssh string. */
/*
 *   string key_blob (55 = 4 + 51 octets)
 *
 * where key_blob is the ssh encoding of
 *
 *   string "ssh-ed25519"     (15 = 4 + 11 octets)
 *   string key               (36 = 4 + 32 octets)
 */
#define SSH_ED25519_PUBKEY_LENGTH 55
#define SSH_ED25519_PUBKEY_BLOB_OFFSET 4
#define SSH_ED25519_PUBKEY_KEY_OFFSET 23

static const uint8_t ssh_public_key_prefix[23]
= "\0\0\0\x33\0\0\0\x0bssh-ed25519\0\0\0\x20"; /* Total length 55 */
static const uint8_t ssh_sign_response_prefix[28]
= "\0\0\0\x58\x0e\0\0\0\x53\0\0\0\x0bssh-ed25519\0\0\0\x40";

static void __attribute__((__format__(__printf__, 1, 2)))
die(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);

  exit(EXIT_FAILURE);
}

static void __attribute__((__format__(__printf__, 1, 2)))
werror(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
}

/* Return 1 on success, 0 on error, -1 on eof */
static int
read_line(struct nettle_buffer *buffer, FILE *f)
{
  int c;

  while ((c = getc(f)) != EOF)
    {
      if (!NETTLE_BUFFER_PUTC(buffer, c))
	return 0;

      if (c == '\n')
	return 1;
    }
  if (ferror(f))
    {
      werror("Read failed: %s\n", strerror(errno));
      return 0;
    }

  else
    return -1;
}

static const uint8_t
pem_start_pattern[11] = "-----BEGIN ";

static const uint8_t
pem_end_pattern[9] = "-----END ";

static const uint8_t
pem_trailer_pattern[5] = "-----";

static const char
pem_ws[33] = {
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 1, 1, 1, 1, 1, 0, 0, /* \t, \n, \v, \f, \r */
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  1 /* SPC */
};

#define PEM_IS_SPACE(c) ((c) < sizeof(pem_ws) && pem_ws[(c)])

/* Returns 1 on match, otherwise 0. */
static int
match_pem_start(size_t length, const uint8_t *line,
		size_t *marker_start,
		size_t *marker_length)
{
  while (length > 0 && PEM_IS_SPACE(line[length - 1]))
    length--;

  if (length > (sizeof(pem_start_pattern) + sizeof(pem_trailer_pattern))
      && memcmp(line, pem_start_pattern, sizeof(pem_start_pattern)) == 0
      && memcmp(line + length - sizeof(pem_trailer_pattern),
		pem_trailer_pattern, sizeof(pem_trailer_pattern)) == 0)
    {
      *marker_start = 11;
      *marker_length = length - (sizeof(pem_start_pattern) + sizeof(pem_trailer_pattern));

      return 1;
    }
  else
    return 0;
}

/* Returns 1 on match, -1 if the line is of the right form except for
   the marker, otherwise 0. */
static int
match_pem_end(size_t length, const uint8_t *line,
	      size_t marker_length,
	      const uint8_t *marker)
{
  while (length > 0 && PEM_IS_SPACE(line[length - 1]))
    length--;

  if (length > (sizeof(pem_end_pattern) + sizeof(pem_trailer_pattern))
      && memcmp(line, pem_end_pattern, sizeof(pem_end_pattern)) == 0
      && memcmp(line + length - sizeof(pem_trailer_pattern),
		pem_trailer_pattern, sizeof(pem_trailer_pattern)) == 0)
    {
      /* Right form. Check marker */
      if (length == marker_length + (sizeof(pem_end_pattern) + sizeof(pem_trailer_pattern))
	  && memcmp(line + sizeof(pem_end_pattern), marker, marker_length) == 0)
	return 1;
      else
	return -1;
    }
  else
    return 0;
}

struct pem_info
{
  /* The FOO part in "-----BEGIN FOO-----" */
  size_t marker_start;
  size_t marker_length;
  size_t data_start;
  size_t data_length;
};

static int
read_pem(struct nettle_buffer *buffer, FILE *f,
	 struct pem_info *info)
{
  /* Find start line */
  for (;;)
    {
      int res;

      nettle_buffer_reset(buffer);

      res = read_line(buffer, f);
      if (res != 1)
	return res;

      if (match_pem_start(buffer->size, buffer->contents,
			  &info->marker_start, &info->marker_length))
	break;
    }

  /* NUL-terminate the marker. Don't care to check for embedded NULs. */
  buffer->contents[info->marker_start + info->marker_length] = 0;

  info->data_start = buffer->size;

  for (;;)
    {
      size_t line_start = buffer->size;

      if (read_line(buffer, f) != 1)
	return 0;

      switch (match_pem_end(buffer->size - line_start,
			    buffer->contents + line_start,
			    info->marker_length,
			    buffer->contents + info->marker_start))
	{
	case 0:
	  break;
	case -1:
	  werror("PEM END line doesn't match BEGIN.\n");
	  return 0;
	case 1:
	  /* Return base 64 data; let caller do the decoding */
	  info->data_length = line_start - info->data_start;
	  return 1;
	}
    }
}

static inline int
base64_decode_in_place (struct base64_decode_ctx *ctx, size_t *dst_length,
			size_t length, uint8_t *data)
{
  return base64_decode_update (ctx, dst_length,
			       data, length, (const char *) data);
}

static int
decode_base64(struct nettle_buffer *buffer,
	      size_t start, size_t *length)
{
  struct base64_decode_ctx ctx;

  base64_decode_init(&ctx);

  /* Decode in place */
  if (base64_decode_in_place(&ctx, length,
			     *length, buffer->contents + start)
      && base64_decode_final(&ctx))
    return 1;

  else
    {
      werror("Invalid base64 date.\n");
      return 0;
    }
}

static int
parse_openssh_private_key(uint8_t *priv, uint8_t *pub,
			  size_t length, const uint8_t *data)
{
  static const uint8_t private_key_prefix[39]
    = "openssh-key-v1\0\0\0\0\4none\0\0\0\4none\0\0\0\0\0\0\0\1";
  const uint8_t *pubkey_string;
  uint32_t encrypted_length, n1, n2;
  uint8_t reconstructed_pubkey[ED25519_KEY_SIZE];

#define EXTRACT(l, p, msg) do {			\
    if (length < l) {				\
      werror(msg); return 0;			\
    }						\
    memcpy(p, data, l);				\
    length -= l; data += l;			\
  } while(0)
#define SKIP(l, p, msg) do {				\
    if (length < l || memcmp(data, p, l) != 0) {	\
      werror(msg); return 0;				\
    }							\
    length -= l; data += l;				\
  } while(0)
#define UINT32(x) do {					\
      if (length < 4) {					\
	werror("Truncated key blob\n"); return 0;	\
      }							\
      x = READ_UINT32(data);				\
      length -= 4; data += 4;				\
    } while(0)
#define SKIP_UINT32(x, msg) do {		\
      uint32_t got_x;				\
      UINT32(got_x);				\
      if (got_x != x) {				\
	werror(msg); return 0;			\
      }						\
    } while(0)
    SKIP(sizeof(private_key_prefix), private_key_prefix, "Invalid (or encrypted) key\n");

    pubkey_string = data;

    SKIP(sizeof(ssh_public_key_prefix), ssh_public_key_prefix, "Invalid pubkey blob\n");
    EXTRACT(ED25519_KEY_SIZE, pub, "Truncated public key blob\n");
    UINT32(encrypted_length);
    if (encrypted_length != length || encrypted_length % 8 != 0)
      {
	werror("Invalid private key blob\n");
	return 0;
      }
    UINT32(n1);
    UINT32(n2);
    if (n1 != n2)
      {
	werror("Invalid private key nonces\n");
	return 0;
      }
    SKIP(51, pubkey_string + SSH_ED25519_PUBKEY_BLOB_OFFSET,
	 "Inconsistent pubkey blob\n");
    SKIP_UINT32(64, "Invalid inner private key blob\n");
    EXTRACT(ED25519_KEY_SIZE, priv, "Truncated private key\n");
    SKIP(ED25519_KEY_SIZE, pub, "Inconsistent inner pubkey\n");

    ed25519_sha512_public_key(reconstructed_pubkey, priv);
    if (memcmp(reconstructed_pubkey, pub, ED25519_KEY_SIZE) != 0)
      {
	werror("Inconsistent private key\n");
	return 0;
      }
    /* Ignore any trailing data, including key comment. */

    return 1;
#undef SKIP
#undef EXTRACT
#undef UINT32
#undef SKIP_UINT32
}

static int
read_openssh_private_key(uint8_t *priv, uint8_t *pub, const char *filename)
{
  struct nettle_buffer buffer;
  struct pem_info info;
  int res;
  FILE *f;

  f = fopen(filename, "r");
  if (!f)
    {
      werror("Failed to open file: %s\n", filename);
      return 0;
    }
  nettle_buffer_init_realloc(&buffer, NULL, nettle_xrealloc);

  res = read_pem(&buffer, f, &info);
  fclose(f);

  if (!res)
    {
    fail:
      nettle_buffer_clear(&buffer);
      return 0;
    }
  if (info.marker_length != 19
      || memcmp(buffer.contents + info.marker_start, "OPENSSH PRIVATE KEY", 19) != 0)
    {
      werror("Unexpected pem type");
      goto fail;
    }

  if (!decode_base64(&buffer, info.data_start, &info.data_length))
    goto fail;

  if (!parse_openssh_private_key(priv, pub, info.data_length,
				 buffer.contents + info.data_start))
    goto fail;

  nettle_buffer_clear(&buffer);
  return 1;
}

static int
listen_socket(const char *name)
{
  struct sockaddr_un sa;
  size_t len = strlen(name);
  mode_t old_umask;
  int fd;
  int res;
  sa.sun_family = AF_UNIX;
  if (len >= sizeof(sa.sun_path))
    {
      werror("Socket name too long.\n");
      return -1;
    }
  memcpy(sa.sun_path, name, len);
  sa.sun_path[len] = 0;

  /* Remove name if it already exists. */
  if (unlink(name) < 0 && errno != ENOENT)
    {
      werror("Failed removing old socket: %s\n", strerror(errno));
      return -1;
    }

  fd = socket(AF_UNIX, SOCK_STREAM, 0);
  if (fd < 0)
    {
      werror("Failed creating socket: %s\n", strerror(errno));
      return -1;
    }
  /* We have to change the umask, as that's the only way to control
   * the permissions that bind uses. */
  old_umask = umask(077);
  res = bind(fd, (const struct sockaddr*) &sa, sizeof(sa));
  umask(old_umask);
  if (res < 0)
    {
      werror("bind failed: %s\n", strerror(errno));
      close(fd);
      return -1;
    }
  if (listen(fd, 2) < 0)
    {
      werror("listen failed: %s\n", strerror(errno));
      close(fd);
      return -1;
    }
  return fd;
}

static void
set_nonblocking(int fd)
{
  int old = fcntl(fd, F_GETFL);

  if (old < 0)
    {
      werror("set_nonblocking: fcntl(F_GETFL) failed: %s\n", strerror(errno));
      return;
    }
  if (fcntl(fd, F_SETFL, old | O_NONBLOCK) < 0)
    werror("set_nonblocking: fcntl(F_SETFL) failed: %s\n", strerror(errno));
}

/* Reads a request of at most MAX_REQUEST_SIZE octets. Returns 0 on
   error (since an empty request isn't valid. */
static size_t
recv_request(int fd, size_t max_size, uint8_t *buf)
{
  uint8_t header[4];
  size_t i;
  size_t length;
  for (i = 0; i < 4;)
    {
      int res = read(fd, header + i, 4 - i);
      if (res < 0)
	{
	  if (errno == EINTR)
	    continue;
	  werror("read failed: %s\n", strerror(errno));
	  return 0;
	}
      else if (res == 0)
	{
	  if (i > 0)
	    werror("truncated request header\n");
	  return 0;
	}
      i += res;
    }
  length = READ_UINT32(header);
  if (length > max_size)
    {
      werror("request too large: %zd\n", length);
      return 0;
    }
  for (i = 0; i < length;)
    {
      int res = read(fd, buf + i, length - i);
      if (res < 0)
	{
	  if (errno == EINTR)
	    continue;
	  werror("read failed: %s\n", strerror(errno));
	  return 0;
	}
      else if (res == 0)
	{
	  if (i > 0)
	    werror("truncated request\n");
	  return 0;
	}
      i += res;
    }
  return length;
}

/* Writes pre-formatted response, including length header. */
static int
send_response(int fd, size_t length, const uint8_t *buf)
{
  size_t i;
  for (i = 0; i < length;)
    {
      int res = write(fd, buf + i, length - i);
      if (res < 0)
	{
	  if (errno == EINTR)
	    continue;
	  werror("write failed: %s\n", strerror(errno));
	  return 0;
	}
      i += res;
    }
  return length;
}

static void
handle_requests (const char *socket_name, int listen_fd, int exit_fd, const uint8_t *priv, const uint8_t *pub)
{
  /* Accept only a single connection. */
  int fd, nfds;
  set_nonblocking(listen_fd);
  set_nonblocking(exit_fd);

  nfds = listen_fd + 1;
  if (exit_fd >= nfds)
    nfds = exit_fd + 1;

  for (;;)
    {
      fd_set read_fds;
      int res;
      FD_ZERO(&read_fds);
      FD_SET(listen_fd, &read_fds);
      FD_SET(exit_fd, &read_fds);

      res = select(nfds, &read_fds, NULL, NULL, NULL);
      if (res < 0)
	{
	  if (errno == EINTR)
	    continue;
	  werror("select failed: %s\n", strerror(errno));
	  return;
	}
      if (FD_ISSET(exit_fd, &read_fds))
	{
	  werror("exiting due to exit_fd\n");
	  return;
	}
      if (FD_ISSET(listen_fd, &read_fds))
	{
	  fd = accept(listen_fd, NULL, NULL);
	  if (fd >= 0)
	    break;
	  if (errno == EINTR || errno == EWOULDBLOCK)
	    continue;

	  werror("accept failed: %s\n", strerror(errno));
	  return;
	}
      werror("select returned %d, and no fd set???\n", res);
    }
  /* Serve the connected fd only. */
  close(listen_fd);
  close(exit_fd);
  unlink(socket_name);
  /* TODO: On BSD, may need to explicitly set the connected socket to blocking mode. */

  for (;;)
    {
      uint8_t req[MAX_REQUEST_SIZE];
      size_t length = recv_request(fd, sizeof(req), req);
      static const uint8_t failure[5] = {0,0,0,1, SSH_AGENT_FAILURE };
      if (!length)
	return;
      switch (req[0])
	{
	default:
	failure:
	  if (!send_response(fd, sizeof(failure), failure))
	    return;
	  break;
	case SSH_AGENTC_REQUEST_IDENTITIES:
	  {
	    uint8_t response[9 + SSH_ED25519_PUBKEY_LENGTH + 4]
	      = {0,0,0,5 + SSH_ED25519_PUBKEY_LENGTH + 4, SSH_AGENT_IDENTITIES_ANSWER, 0,0,0,1, };
	    if (length != 1)
	      goto failure;
	    memcpy(response + 9, ssh_public_key_prefix, sizeof(ssh_public_key_prefix));
	    memcpy(response + 9 + sizeof(ssh_public_key_prefix), pub, ED25519_KEY_SIZE);
	    memset(response + 9 + sizeof(ssh_public_key_prefix) + ED25519_KEY_SIZE, 0, 4);
	    if (!send_response(fd, sizeof(response), response))
	      return;
	    break;
	  }
	case SSH_AGENTC_SIGN_REQUEST:
	  {
	    size_t data_length;

	    uint8_t response[sizeof(ssh_sign_response_prefix) + ED25519_SIGNATURE_SIZE];
	    if (length < 1 + SSH_ED25519_PUBKEY_LENGTH + 8)
	      goto failure;
	    if (memcmp(req + 1, ssh_public_key_prefix, sizeof(ssh_public_key_prefix)) != 0)
	      goto failure;
	    if (memcmp(req + 1 + sizeof(ssh_public_key_prefix), pub, ED25519_KEY_SIZE) != 0)
	      goto failure;
	    data_length = READ_UINT32(req + 1 + SSH_ED25519_PUBKEY_LENGTH);
	    if (data_length != length - 1 + SSH_ED25519_PUBKEY_LENGTH + 8)
	      goto failure;
	    memcpy(response, ssh_sign_response_prefix, sizeof(ssh_sign_response_prefix));

	    ed25519_sha512_sign(pub, priv,
				data_length, req + 1 + SSH_ED25519_PUBKEY_LENGTH + 4,
				response + sizeof(ssh_sign_response_prefix));

	    if (!send_response(fd, sizeof(response), response))
	      return;

	    break;
	  }
	}
    }
}

int
main(int argc, char **argv)
{
  const char *key = NULL;
  const char *socket_name = NULL;
  uint8_t priv[ED25519_KEY_SIZE];
  uint8_t pub[ED25519_KEY_SIZE];
  int listen_fd;
  int pipe_fd[2]; /* read-end, write-end */
  pid_t pid;
  int c;
  while ((c = getopt(argc, argv, "k:s:")) != -1)
    switch (c)
      {
      case 'k':
	key = optarg;
	break;
      case 's':
	socket_name = optarg;
	break;
      default:
      usage:
	die("Usage: %s -k key-file command...\n", argv[0]);
      }
  if (!key || !socket_name || optind >= argc)
    goto usage;

  if (!read_openssh_private_key(priv, pub, key))
    return EXIT_FAILURE;

  listen_fd = listen_socket(socket_name);
  if (listen_fd < 0)
    return EXIT_FAILURE;

  /* Fork, and let the *child* process act as agent. It will exit
     after one connection to agent. To have it not linger, if none
     ever connects, also open a separate pipe between parent and
     child, and have agent process exit on EOF from this pipe.
  */
  if (pipe(pipe_fd) < 0)
    die("pipe failed: %s\n", strerror(errno));
  pid = fork();
  if (pid < 0)
    die("fork failed: %s\n", strerror(errno));
  else if (pid == 0)
    {
      /* Child process, to act as agent. */
      int null_fd = open("/dev/null", O_RDWR);
      if (null_fd < 0)
	die("open /dev/null failed: %s\n", strerror(errno));

      if (dup2(null_fd, STDIN_FILENO) < 0)
	die("dup2 to stdin failed: %s\n", strerror(errno));
      if (dup2(null_fd, STDOUT_FILENO)< 0)
	die("dup2 to stdout failed: %s\n", strerror(errno));

      close(pipe_fd[1]);
      close(null_fd);
      handle_requests(socket_name, listen_fd, pipe_fd[0], priv, pub);
      _exit(0);
    }
  else
    {
      werror("spawned agent process %ld\n", (long) pid);
      close(pipe_fd[0]);
      close(listen_fd);
      if (setenv("SSH_AUTH_SOCK", socket_name, 1) < 0)
	die("setenv: %s\n", strerror(errno));
      if (execvp (argv[optind], argv + optind) < 0)
	die("execvp failed: %s\n", strerror(errno));
    }
}
