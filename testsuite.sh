#! /bin/sh

set -e

die() {
    echo "$@" >&2
    exit 1
}

rm -f test.key test.key.pub
ssh-keygen -q -N '' -C '' -t ed25519 -f test.key

echo 1>&2 Testing ssh-add -L
./test-agent -k ./test.key -s test.sock -- ssh-add -L > test.out
diff -u test.key.pub test.out || die Public key not listed by ssh-add -L

echo 1>&2 Testing ssh-keygen -Y sign
echo foo > test.msg
rm -f test.msg.sig
./test-agent -k ~/hack/sigsum-go/tests/test.key -s test.sock -- \
  ssh-keygen -Y sign -f test.key.pub -n test test.msg \
    || die "ssh-keygen -Y sign failed"

ssh-keygen -Y check-novalidate -f test.key.pub -n test -s test.msg.sig < test.msg
